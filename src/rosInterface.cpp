/**************************************
 * Title    : rosInterface.cpp
 * Author   : Ian Gagnon
 * Created  : 2021-05-05
 *************************************/

#include "../include/rosInterface.h"


rosInterface::rosInterface(): sub_("arduino_motors", &rosInterface::callback, this), pub_("arduino_sensors", &msg_) {
}


rosInterface::~rosInterface() {
}


void rosInterface::setup() {
  nh_.initNode();
  float data[N_DATA_TO_PUBLISH];
  msg_.data = data;
  msg_.data_length = N_DATA_TO_PUBLISH;
  nh_.subscribe(sub_);
  nh_.advertise(pub_);
}


void rosInterface::callback(const std_msgs::Float32MultiArray& msg) {
  for (int i=0; i<N_DATA_TO_RECEIVE; i++)
    steps_[i] = msg.data[i];
  newCommandReceived_ = true;
}


bool rosInterface::newCommandReceived() {
  return newCommandReceived_;
}


float* rosInterface::getCommand() {
  return steps_;
}


void rosInterface::publish(float* arrayToPublish) {
  for (int i=0; i<N_DATA_TO_PUBLISH; i++){
    msg_.data[i] = arrayToPublish[i];
  }
  pub_.publish(&msg_);
}


void rosInterface::main() {
  nh_.spinOnce();
}
