# Projet sixi-firmware

Programme de contrôle des moteurs du bras robotique Sixi 2.

Platforme : Arduino

## Description générale du projet

Le bras robotique Sixi 2 comprend un microcontrôleur Arduino, qui permet de contrôler les moteurs qui sont sur le bras, ainsi que de lire les données des capteurs de position de chaque joint du bras. Le microcontrôleur peut recevoir des commandes et faire bouger le bras robotique selon ces commandes. Les commandes proviennent d'un programme ROS exécuté sur un Raspberry Pi.

## Structure des fichiers

```plaintext
sixi-firmware/
|-- include/
|   |-- ...
|
|-- src/
|   |-- ...
|
|-- tests/
|   |-- ...
|
|-- main.ino
```

Le fichier `main.ino` est le fichier principal du programme du firmware. Il utilise les fichiers source qui se trouvent dans les dossiers `include` et `src`.

Le dossier `tests` contient des fichiers qui peuvent être lancés à la place de `main.ino` et qui permettent de tester une partie du firmware uniquement.

## Travailler sur ce projet

Consulter le tutoriel suivant pour voir comment utiliser Visual Studio Code pour programmer dans ce projet : https://docs.google.com/document/d/1Pk5eunK2JzD9pLcS8nHgKEZ8-MSgKCscXjAQoR15zpk/edit. Vous pouvez aussi utiliser le classique Arduino IDE.